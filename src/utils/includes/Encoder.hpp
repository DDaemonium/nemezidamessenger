#pragma once

#include <string>

namespace utils {

	/**
	 * Abstract class Encoder.
	 * Gives an interface for the implementation of encryptors different algorithms.
	 */
	class Encoder {
		std::string _key;

	public:
		Encoder() = default;
		Encoder(const Encoder&) = default;
		Encoder(Encoder&&) = default;

		Encoder& operator = (const Encoder&) = default;
		Encoder& operator = (Encoder&&) = default;

		virtual ~Encoder() = default;

		/// Set key for using in encode and decode operations
		virtual void setKey(const std::string& key) noexcept(false) = 0;

		/// Encode source string with given key
		virtual std::wstring encode(const std::wstring& src) noexcept(false) = 0;

		/// Encode source string with other key
		virtual std::wstring encode(const std::wstring& src, const std::string& key) noexcept(false) = 0;

		/// Decode source string with given key
		virtual std::wstring decode(const std::wstring& src) noexcept(false) = 0;

		/// Decode source string with other key
		virtual std::wstring decode(const std::wstring& src, const std::string& key) noexcept(false) = 0;
	};

}